﻿using StartCoding.Settings;
using StartCoding.Views.Admin;
using StartCoding.Views.Customer;
using System.Windows;
using System.Windows.Controls;

namespace StartCoding
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.WindowState = WindowState.Maximized;
        }

        string adminEmail = "1";
        string adminPassword = "1";
        string customerEmail = "2";
        string customerPassword = "2";

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            var checkAdmin = tfEmail.Text.Equals(adminEmail) && tfPassword.Text.Equals(adminPassword);
            var checkCustomer = tfEmail.Text.Equals(customerEmail) && tfPassword.Text.Equals(customerPassword);
            var checkLogOut = false;

            if (checkAdmin)
            {
                AdminLogin();
            }
            else if (checkCustomer)
            {
                CustomerLogin();
            }
            else if (checkLogOut)
            {
                MessageBox.Show("Log Out");
            }
            else
            {
                MessageBox.Show("Login Fail. Check Email and Password.");
            }
        }

        public void AdminLogin()
        {
            var data = new AdminLayout();
            UIFlow.Switch(pMainWindow, data);
        }

        public void CustomerLogin()
        {
            var data = new BillsLayout();
            UIFlow.Switch(pMainWindow, data);
        }
    }
}
