﻿using StartCoding.ObjectType;
using StartCoding.Settings;
using StartCoding.Views.Admin.AccountType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StartCoding.Views.Admin.Discounts
{
    /// <summary>
    /// Interaction logic for DiscountLayout.xaml
    /// </summary>
    public partial class DiscountLayout : UserControl
    {
        public DiscountLayout()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            UIFlow.AddPane(UIID.DiscountAddNewPane, cDiscountsAddNew);
            UIFlow.AddPane(UIID.DiscountViewAllPane, cDiscountsViewAll);
        }
    }
}
