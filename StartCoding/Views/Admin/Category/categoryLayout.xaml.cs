﻿using StartCoding.ObjectType;
using StartCoding.Settings;
using System.Windows.Controls;

namespace StartCoding.Views.Admin.Category
{
    /// <summary>
    /// Interaction logic for CategoryLayout.xaml
    /// </summary>
    public partial class CategoryLayout : UserControl
    {
        public CategoryLayout()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            UIFlow.AddPane(UIID.CategoryAddNewPane, cCategoryAddNew);
            UIFlow.AddPane(UIID.CategoryViewAllPane, cCategoryViewAll);
        }
    }
}
