﻿using StartCoding.ObjectType;
using StartCoding.Settings;
using StartCoding.Views.Admin.AccountType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StartCoding.Views.Admin.AccountStatus
{
    /// <summary>
    /// Interaction logic for AccountStatusLayout.xaml
    /// </summary>
    public partial class AccountStatusLayout : UserControl
    {
        public AccountStatusLayout()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            UIFlow.AddPane(UIID.AccountStatusAddNewPane, cAccountStatusAddNew);
            UIFlow.AddPane(UIID.AccountStatusViewAllPane, cAccountStatusViewAll);
        }
    }
}
