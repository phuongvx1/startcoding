﻿using StartCoding.ObjectType;
using StartCoding.Settings;
using System.Windows.Controls;

namespace StartCoding.Views.Admin.ProductStatus
{
    /// <summary>
    /// Interaction logic for ProductsStatusLayout.xaml
    /// </summary>
    public partial class ProductsStatusLayout : UserControl
    {
        public ProductsStatusLayout()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            UIFlow.AddPane(UIID.ProductStatusAddNewPane, cProductStatusAddNew);
            UIFlow.AddPane(UIID.ProductStatusViewAllPane, cProductStatusViewAll);
        }
    }
}
