﻿using StartCoding.ObjectType;
using StartCoding.Settings;
using StartCoding.Views.Admin.AccountType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StartCoding.Views.Admin.BillStatus
{
    /// <summary>
    /// Interaction logic for BillStatusLayout.xaml
    /// </summary>
    public partial class BillStatusLayout : UserControl
    {
        public BillStatusLayout()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            UIFlow.AddPane(UIID.BillStatusAddNewPane, cBillStatusAddNew);
            UIFlow.AddPane(UIID.BillStatusViewAllPane, cBillStatusViewAll);
        }
    }
}
