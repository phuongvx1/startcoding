﻿using StartCoding.Models.Ado;
using StartCoding.Settings;
using StartCoding.Views.Admin.Dashboard;
using StartCoding.Views.Admin.Products;
using StartCoding.Views.Customer;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Convert = StartCoding.Settings.Convert;

namespace StartCoding.Views.Admin
{
    /// <summary>
    /// Interaction logic for AdminLayout.xaml
    /// </summary>
    public partial class AdminLayout : UserControl
    {
        public AdminLayout()
        {
            InitializeComponent();
        }

        public void LabelOnClick(object sender, RoutedEventArgs e)
        {
            UIFlow.UIControler(Convert.GetObjectType(sender.ToString()), pView);
        }
    }
}
