﻿using StartCoding.ObjectType;
using StartCoding.Settings;
using StartCoding.Views.Admin.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StartCoding.Views.Admin.AccountType
{
    /// <summary>
    /// Interaction logic for AccountTypeLayout.xaml
    /// </summary>
    public partial class AccountTypeLayout : UserControl
    {
        public AccountTypeLayout()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            UIFlow.AddPane(UIID.AccountTypeAddNewPane, cAccountTypeAddNew);
            UIFlow.AddPane(UIID.AccountTypeViewAllPane, cAccountTypeViewAll);
        }
    }
}
