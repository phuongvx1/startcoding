﻿using StartCoding.ObjectType;
using StartCoding.Settings;
using System.Windows.Controls;

namespace StartCoding.Views.Admin.ProductLevel
{
    /// <summary>
    /// Interaction logic for ProductLevelLayout.xaml
    /// </summary>
    public partial class ProductLevelLayout : UserControl
    {
        public ProductLevelLayout()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            UIFlow.AddPane(UIID.ProductLevelAddNewPane, cProductLevelAddNew);
            UIFlow.AddPane(UIID.ProductLevelViewAllPane, cProductLevelViewAll);
        }
    }
}
