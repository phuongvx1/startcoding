﻿using StartCoding.ObjectType;
using StartCoding.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StartCoding.Views.Admin.Bills
{
    /// <summary>
    /// Interaction logic for BillsLayout.xaml
    /// </summary>
    public partial class BillsLayout : UserControl
    {
        public BillsLayout()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            UIFlow.AddPane(UIID.BillsViewAllPane, cBillViewAll);
            UIFlow.AddPane(UIID.BillStatusLayoutPane, cBillStatus);
            UIFlow.AddPane(UIID.DiscountLayoutPane, cDiscounts);
            UIFlow.AddPane(UIID.PaymentModeLayoutPane, cPaymentMode);
        }
    }
}
