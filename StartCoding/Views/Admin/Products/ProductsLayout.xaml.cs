﻿using StartCoding.ObjectType;
using StartCoding.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StartCoding.Views.Admin.Products
{
    /// <summary>
    /// Interaction logic for ProductsLayout.xaml
    /// </summary>
    public partial class ProductsLayout : UserControl
    {
        public ProductsLayout()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            UIFlow.AddPane(UIID.ProductAddNewPane,cProductAddNew);
            UIFlow.AddPane(UIID.ProductViewAllPane, cProductViewAll);
            UIFlow.AddPane(UIID.CategoryLayoutPane, cCategory);
            UIFlow.AddPane(UIID.ProductLevelLayoutPane, cProductLevel);
            UIFlow.AddPane(UIID.ProductStatusLayoutPane, cProductStatus);
        }
    }
}
