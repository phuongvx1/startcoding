﻿using Microsoft.EntityFrameworkCore.Internal;
using StartCoding.Models.Ado;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;
using System.Windows.Markup;

namespace StartCoding.API
{
    public class ServerService
    {
        public static bool Insert<T>(API api, T data)
        {
            var check = false;
            switch (api)
            {
                case API.AccountInsert:
                    using (var service = new startcodingEntities())
                    {
                        var tmp = data as object as account;
                        if(tmp != null && !isExists(API.AccountInsert, tmp.account_email) && !isExists(API.AccountInsert, tmp.account_contact))
                            service.accounts.AddObject(tmp);
                    }
                    break;

                case API.AccountLoginStatusInsert:
                    using (var service = new startcodingEntities())
                    {
                        var tmp = data as object as account_login_status;
                        if (tmp != null && !isExists(API.AccountLoginStatusInsert, tmp.account_login_status_name))
                            service.account_login_status.AddObject(tmp);
                    }
                    break;

                case API.AccountStatusInsert:
                    using (var service = new startcodingEntities())
                    {
                        var tmp = data as object as account_status;
                        if (tmp != null && !isExists(API.AccountStatusInsert, tmp.account_status_name))
                            service.account_status.AddObject(tmp);
                    }
                    break;

                case API.AccountTypeInsert:
                    using (var service = new startcodingEntities())
                    {
                        var tmp = data as object as account_type;
                        if (tmp != null && !isExists(API.AccountTypeInsert, tmp.account_type_name))
                            service.account_type.AddObject(tmp);
                    }
                    break;

                case API.BillsInsert:
                    using (var service = new startcodingEntities())
                    {
                        var tmp = data as object as bill;
                        if (tmp != null && !isExists(API.BillsInsert, tmp.bill_time.ToString()))
                            service.bills.AddObject(tmp);
                    }
                    break;

                case API.BillsStatusInsert:
                    using (var service = new startcodingEntities())
                    {
                        var tmp = data as object as bill_status;
                        if (tmp != null && !isExists(API.BillsStatusInsert, tmp.bill_status_name))
                            service.bill_status.AddObject(tmp);
                    }
                    break;

                case API.CategoryInsert:
                    using (var service = new startcodingEntities())
                    {
                        var tmp = data as object as category;
                        if (tmp != null && !isExists(API.CategoryInsert, tmp.category_name))
                            service.categories.AddObject(tmp);
                    }
                    break;

                case API.DiscountInsert:
                    using (var service = new startcodingEntities())
                    {
                        var tmp = data as object as discount;
                        if (tmp != null && !isExists(API.DiscountInsert, tmp.discount_name))
                            service.discounts.AddObject(tmp);
                    }
                    break;

                case API.PaymentModeInsert:
                    using (var service = new startcodingEntities())
                    {
                        var tmp = data as object as payment_mode;
                        if (tmp != null && !isExists(API.PaymentModeInsert, tmp.payment_mode_time.ToString()))
                            service.payment_mode.AddObject(tmp);
                    }
                    break;

                case API.ProductLevelInsert:
                    using (var service = new startcodingEntities())
                    {
                        var tmp = data as object as product_level;
                        if (tmp != null && !isExists(API.ProductLevelInsert, tmp.product_level_name))
                            service.product_level.AddObject(tmp);
                    }
                    break;

                case API.ProductInsert:
                    using (var service = new startcodingEntities())
                    {
                        var tmp = data as object as product;
                        if (tmp != null && !isExists(API.AccountInsert, tmp.product_title))
                            service.products.AddObject(tmp);
                    }
                    break;

                case API.ProductStatusInsert:
                    using (var service = new startcodingEntities())
                    {
                        var tmp = data as object as product_status;
                        if (tmp != null && !isExists(API.ProductStatusInsert, tmp.product_status_name))
                            service.product_status.AddObject(tmp);
                    }
                    break;
            }

            return check;
        }

        public static void Update<T>(API api, T data)
        {
            switch (api)
            {
                case API.AccountUpdate:
                    using (var service = new startcodingEntities())
                    {
                        var currentData = data as object as account;
                        var updateData = service.accounts.First(tmp => tmp.account_id == currentData.account_id);
                        updateData.account_name = currentData.account_name;
                        updateData.account_email = currentData.account_email;
                        updateData.account_address = currentData.account_address;
                        updateData.account_type = currentData.account_type;
                        updateData.account_contact = currentData.account_contact;
                        updateData.account_status = currentData.account_status;
                        service.SaveChanges();
                    }
                    break;

                case API.AccountLoginStatusUpdate:
                    using (var service = new startcodingEntities())
                    {
                        var currentData = data as object as account_login_status;
                        var updateData = service.account_login_status.First(tmp => tmp.account_login_status_id == currentData.account_login_status_id);
                        updateData.account_login_status_name = currentData.account_login_status_name;
                        service.SaveChanges();
                    }
                    break;

                case API.AccountStatusUpdate:
                    using (var service = new startcodingEntities())
                    {
                        var currentData = data as object as account_status;
                        var updateData = service.account_status.First(tmp => tmp.account_status_id == currentData.account_status_id);
                        updateData.account_status_name = currentData.account_status_name;
                        service.SaveChanges();
                    }
                    break;

                case API.AccountTypeUpdate:
                    using (var service = new startcodingEntities())
                    {
                        var currentData = data as object as account_type;
                        var updateData = service.account_type.First(tmp => tmp.account_type_id == currentData.account_type_id);
                        updateData.account_type_name = currentData.account_type_name;
                        service.SaveChanges();
                    }
                    break;

                case API.BillsUpdate:
                    using (var service = new startcodingEntities())
                    {
                        var currentData = data as object as bill;
                        var updateData = service.bills.First(tmp => tmp.bill_id == currentData.bill_id);
                        updateData.bill_payment = currentData.bill_payment;
                        updateData.payment_mode_id = currentData.payment_mode_id;
                        updateData.bill_status_id = currentData.bill_status_id;
                        updateData.discount_id = currentData.discount_id;
                        service.SaveChanges();
                    }
                    break;

                case API.BillsStatusUpdate:
                    using (var service = new startcodingEntities())
                    {
                        var currentData = data as object as bill_status;
                        var updateData = service.bill_status.First(tmp => tmp.bill_status_id == currentData.bill_status_id);
                        updateData.bill_status_name = currentData.bill_status_name;
                        service.SaveChanges();
                    }
                    break;

                case API.CategoryUpdate:
                    using (var service = new startcodingEntities())
                    {
                        var currentData = data as object as category;
                        var updateData = service.categories.First(tmp => tmp.category_id == currentData.category_id);
                        updateData.category_name = currentData.category_name;
                        updateData.category_image = currentData.category_image;
                        service.SaveChanges();
                    }
                    break;

                case API.DiscountUpdate:
                    using (var service = new startcodingEntities())
                    {
                        var currentData = data as object as discount;
                        var updateData = service.discounts.First(tmp => tmp.discount_id == currentData.discount_id);
                        updateData.discount_name = currentData.discount_name;
                        updateData.discount_info = currentData.discount_info;
                        service.SaveChanges();
                    }
                    break;

                case API.PaymentModeUpdate:
                    using (var service = new startcodingEntities())
                    {
                        var currentData = data as object as payment_mode;
                        var updateData = service.payment_mode.First(tmp => tmp.payment_mode_id == currentData.payment_mode_id);
                        updateData.payment_mode_name = currentData.payment_mode_name;
                        updateData.payment_mode_time = currentData.payment_mode_time;
                        service.SaveChanges();
                    }
                    break;

                case API.ProductLevelUpdate:
                    using (var service = new startcodingEntities())
                    {
                        var currentData = data as object as product_level;
                        var updateData = service.product_level.First(tmp => tmp.product_level_id == currentData.product_level_id);
                        updateData.product_level_name = currentData.product_level_name;
                        service.SaveChanges();
                    }
                    break;

                case API.ProductUpdate:
                    using (var service = new startcodingEntities())
                    {
                        var currentData = data as object as product;
                        var updateData = service.products.First(tmp => tmp.product_id == currentData.product_id);
                        updateData.product_title = currentData.product_title;
                        updateData.product_question = currentData.product_question;
                        updateData.product_work_time = currentData.product_work_time;
                        updateData.product_high_score = currentData.product_high_score;
                        updateData.product_current_score = currentData.product_current_score;
                        updateData.product_count_success = currentData.product_count_success;
                        updateData.product_level_id = currentData.product_level_id;
                        service.SaveChanges();
                    }
                    break;

                case API.ProductStatusUpdate:
                    using (var service = new startcodingEntities())
                    {
                        var currentData = data as object as product_status;
                        var updateData = service.product_status.First(tmp => tmp.product_status_id == currentData.product_status_id);
                        updateData.product_status_name = currentData.product_status_name;
                        service.SaveChanges();
                    }
                    break;
            }
        }

        public static List<T> Get<T>(API api, string data = "")
        {
            var ls = new List<T>();

            switch (api)
            {
                case API.AccountGetAll:
                    using (var service = new startcodingEntities())
                    {
                        ls.AddRange((IEnumerable<T>)service.accounts);
                    }
                    break;
                case API.AccountGetActive:
                    using (var service = new startcodingEntities())
                    {
                        var all = (IEnumerable<T>)service.accounts.Where(tmp => tmp.account_status == 1);
                        var off = (IEnumerable<T>)service.accounts.Where(tmp => (tmp.account_email.Contains(data) || tmp.account_contact.Contains(data)) && tmp.account_status == 1);
                        ls.AddRange(data.Equals("") ? all : off);
                    }
                    break;

                case API.AccountLoginStatusGetAll:
                    using (var service = new startcodingEntities())
                    {
                        ls.AddRange((IEnumerable<T>)service.account_login_status);
                    }
                    break;
                case API.AccountLoginStatusGetActive:
                    using (var service = new startcodingEntities())
                    {
                        var all = (IEnumerable<T>)service.account_login_status.Where(tmp => tmp.account_login_status_status == true);
                        var off = (IEnumerable<T>)service.account_login_status.Where(tmp => tmp.account_login_status_name.Contains(data) && tmp.account_login_status_status == true);
                        ls.AddRange(data.Equals("") ? all : off);
                    }
                    break;

                case API.AccountStatusGetAll:
                    using (var service = new startcodingEntities())
                    {
                        ls.AddRange((IEnumerable<T>)service.account_status);
                    }
                    break;
                case API.AccountStatusGetActive:
                    using (var service = new startcodingEntities())
                    {
                        var all = (IEnumerable<T>)service.account_status.Where(tmp => tmp.account_status_status == true);
                        var off = (IEnumerable<T>)service.account_status.Where(tmp => tmp.account_status_name.Contains(data) && tmp.account_status_status == true);
                        ls.AddRange(data.Equals("") ? all : off);
                    }
                    break;

                case API.AccountTypeGetAll:
                    using (var service = new startcodingEntities())
                    {
                        ls.AddRange((IEnumerable<T>)service.account_type);
                    }
                    break;
                case API.AccountTypeGetActive:
                    using (var service = new startcodingEntities())
                    {
                        var all = (IEnumerable<T>)service.account_type.Where(tmp => tmp.account_type_status == true);
                        var off = (IEnumerable<T>)service.account_type.Where(tmp => tmp.account_type_name.Contains(data) && tmp.account_type_status == true);
                        ls.AddRange(data.Equals("") ? all : off);
                    }
                    break;

                case API.BillsGetAll:
                    using (var service = new startcodingEntities())
                    {
                        ls.AddRange((IEnumerable<T>)service.bills);
                    }
                    break;
                case API.BillsGetActive:
                    using (var service = new startcodingEntities())
                    {
                        var all = (IEnumerable<T>)service.bills.Where(tmp => tmp.bill_status_id == 1);
                        var off = (IEnumerable<T>)service.bills.Where(tmp => tmp.bill_time == Convert.ToInt32(data) && tmp.bill_status_id == 1);
                        ls.AddRange(data.Equals("") ? all : off);
                    }
                    break;

                case API.BillsStatusGetAll:
                    using (var service = new startcodingEntities())
                    {
                        ls.AddRange((IEnumerable<T>)service.bill_status);
                    }
                    break;
                case API.BillsStatusGetActive:
                    using (var service = new startcodingEntities())
                    {
                        var all = (IEnumerable<T>)service.bill_status.Where(tmp => tmp.bill_status_status == true);
                        var off = (IEnumerable<T>)service.bill_status.Where(tmp => tmp.bill_status_name.Contains(data) && tmp.bill_status_status == true);
                        ls.AddRange(data.Equals("") ? all : off);
                    }
                    break;

                case API.CategoryGetAll:
                    using (var service = new startcodingEntities())
                    {
                        ls.AddRange((IEnumerable<T>)service.categories);
                    }
                    break;
                case API.CategoryGetActive:
                    using (var service = new startcodingEntities())
                    {
                        var all = (IEnumerable<T>)service.categories.Where(tmp => tmp.category_status == true);
                        var off = (IEnumerable<T>)service.categories.Where(tmp => tmp.category_name.Contains(data) && tmp.category_status == true);
                        ls.AddRange(data.Equals("") ? all : off);
                    }
                    break;

                case API.DiscountGetAll:
                    using (var service = new startcodingEntities())
                    {
                        ls.AddRange((IEnumerable<T>)service.discounts);
                    }
                    break;
                case API.DiscountGetActive:
                    using (var service = new startcodingEntities())
                    {
                        var all = (IEnumerable<T>)service.discounts.Where(tmp => tmp.discount_status == true);
                        var off = (IEnumerable<T>)service.discounts.Where(tmp => (tmp.discount_name.Contains(data) || tmp.discount_start == Convert.ToInt32(data) || tmp.discount_end == Convert.ToInt32(data)) && tmp.discount_status == true);
                        ls.AddRange(data.Equals("") ? all : off);
                    }
                    break;

                case API.PaymentModeGetActive:
                    using (var service = new startcodingEntities())
                    {
                        var all = (IEnumerable<T>)service.payment_mode.Where(tmp => tmp.payment_mode_id == Convert.ToInt32(data) ||
                                                                                    tmp.payment_mode_time == Convert.ToInt32(data) ||
                                                                                    tmp.payer_id.Contains(data) ||
                                                                                    tmp.order_id.Contains(data) ||
                                                                                    tmp.payment_mode_name.Contains(data));
                        ls.AddRange(all);
                    }
                    break;

                case API.ProductLevelGetAll:
                    using (var service = new startcodingEntities())
                    {
                        ls.AddRange((IEnumerable<T>)service.product_level);
                    }
                    break;
                case API.ProductLevelGetActive:
                    using (var service = new startcodingEntities())
                    {
                        var all = (IEnumerable<T>)service.product_level.Where(tmp => tmp.product_level_status == true);
                        var off = (IEnumerable<T>)service.product_level.Where(tmp => tmp.product_level_name.Contains(data) && tmp.product_level_status == true);
                        ls.AddRange(data.Equals("") ? all : off);
                    }
                    break;

                case API.ProductGetAll:
                    using (var service = new startcodingEntities())
                    {
                        ls.AddRange((IEnumerable<T>)service.products);
                    }
                    break;
                case API.ProductGetActive:
                    using (var service = new startcodingEntities())
                    {
                        var all = (IEnumerable<T>)service.products.Where(tmp => tmp.product_status == 1);
                        var off = (IEnumerable<T>)service.products.Where(tmp => (tmp.product_title.Contains(data) ||
                                                                                tmp.product_question.Contains(data)) &&
                                                                                tmp.product_status == 1);
                        ls.AddRange(data.Equals("") ? all : off);
                    }
                    break;

                case API.ProductStatusGetAll:
                    using (var service = new startcodingEntities())
                    {
                        ls.AddRange((IEnumerable<T>)service.product_status);
                    }
                    break;
                case API.ProductStatusGetActive:
                    using (var service = new startcodingEntities())
                    {
                        var all = (IEnumerable<T>)service.product_status.Where(tmp => tmp.product_status_status == true);
                        var off = (IEnumerable<T>)service.product_status.Where(tmp => tmp.product_status_name.Contains(data) && tmp.product_status_status == true);
                        ls.AddRange(data.Equals("") ? all : off);
                    }
                    break;
            }
            return ls;
        }

        public static bool ChangeStatus<T>(API api, T data)
        {
            var check = false;
            switch (api)
            {
                case API.AccountChangeStatus:
                    using (var service = new startcodingEntities())
                    {
                        var dataChangeStatus = data as object as account;
                        var changeStatusData = service.accounts.First(tmp => tmp.account_id == dataChangeStatus.account_id);
                        changeStatusData.account_status = dataChangeStatus.account_status;
                        service.SaveChanges();

                        check = service.accounts.Any(tmp => tmp.account_id == dataChangeStatus.account_id && tmp.account_status == dataChangeStatus.account_status);
                    }
                    break;

                case API.AccountLoginStatusChangeStatus:
                    using (var service = new startcodingEntities())
                    {
                        var dataChangeStatus = data as object as account_login_status;
                        var changeStatusData = service.account_login_status.First(tmp => tmp.account_login_status_id == dataChangeStatus.account_login_status_id);
                        changeStatusData.account_login_status_status = dataChangeStatus.account_login_status_status;
                        service.SaveChanges();

                        check = service.account_login_status.Any(tmp => tmp.account_login_status_id == dataChangeStatus.account_login_status_id && tmp.account_login_status_status == dataChangeStatus.account_login_status_status);
                    }
                    break;

                case API.AccountStatusChangeStatus:
                    using (var service = new startcodingEntities())
                    {
                        var dataChangeStatus = data as object as account_status;
                        var changeStatusData = service.account_status.First(tmp => tmp.account_status_id == dataChangeStatus.account_status_id);
                        changeStatusData.account_status_status = dataChangeStatus.account_status_status;
                        service.SaveChanges();

                        check = service.account_status.Any(tmp => tmp.account_status_id == dataChangeStatus.account_status_id && tmp.account_status_status == dataChangeStatus.account_status_status);
                    }
                    break;

                case API.AccountTypeChangeStatus:
                    using (var service = new startcodingEntities())
                    {
                        var dataChangeStatus = data as object as account_type;
                        var changeStatusData = service.account_type.First(tmp => tmp.account_type_id == dataChangeStatus.account_type_id);
                        changeStatusData.account_type_status = dataChangeStatus.account_type_status;
                        service.SaveChanges();

                        check = service.account_type.Any(tmp => tmp.account_type_id == dataChangeStatus.account_type_id && tmp.account_type_status == dataChangeStatus.account_type_status);
                    }
                    break;

                case API.BillsChangeStatus:
                    using (var service = new startcodingEntities())
                    {
                        var dataChangeStatus = data as object as bill;
                        var changeStatusData = service.bills.First(tmp => tmp.bill_id == dataChangeStatus.bill_id);
                        changeStatusData.bill_status = dataChangeStatus.bill_status;
                        service.SaveChanges();

                        check = service.bills.Any(tmp => tmp.bill_id == dataChangeStatus.bill_id && tmp.bill_status == dataChangeStatus.bill_status);
                    }
                    break;

                case API.BillsStatusChangeStatus:
                    using (var service = new startcodingEntities())
                    {
                        var dataChangeStatus = data as object as bill_status;
                        var changeStatusData = service.bill_status.First(tmp => tmp.bill_status_id == dataChangeStatus.bill_status_id);
                        changeStatusData.bill_status_status = dataChangeStatus.bill_status_status;
                        service.SaveChanges();

                        check = service.bill_status.Any(tmp => tmp.bill_status_id == dataChangeStatus.bill_status_id && tmp.bill_status_status == dataChangeStatus.bill_status_status);
                    }
                    break;

                case API.CategoryChangeStatus:
                    using (var service = new startcodingEntities())
                    {
                        var dataChangeStatus = data as object as category;
                        var changeStatusData = service.categories.First(tmp => tmp.category_id == dataChangeStatus.category_id);
                        changeStatusData.category_status = dataChangeStatus.category_status;
                        service.SaveChanges();

                        check = service.categories.Any(tmp => tmp.category_id == dataChangeStatus.category_id && tmp.category_status == dataChangeStatus.category_status);
                    }
                    break;

                case API.DiscountChangeStatus:
                    using (var service = new startcodingEntities())
                    {
                        var dataChangeStatus = data as object as discount;
                        var changeStatusData = service.discounts.First(tmp => tmp.discount_id == dataChangeStatus.discount_id);
                        changeStatusData.discount_status = dataChangeStatus.discount_status;
                        service.SaveChanges();

                        check = service.discounts.Any(tmp => tmp.discount_id == dataChangeStatus.discount_id && tmp.discount_status == dataChangeStatus.discount_status);
                    }
                    break;

                case API.ProductLevelChangeStatus:
                    using (var service = new startcodingEntities())
                    {
                        var dataChangeStatus = data as object as product_level;
                        var changeStatusData = service.product_level.First(tmp => tmp.product_level_id == dataChangeStatus.product_level_id);
                        changeStatusData.product_level_status = dataChangeStatus.product_level_status;
                        service.SaveChanges();

                        check = service.product_level.Any(tmp => tmp.product_level_id == dataChangeStatus.product_level_id && tmp.product_level_status == dataChangeStatus.product_level_status);
                    }
                    break;

                case API.ProductChangeStatus:
                    using (var service = new startcodingEntities())
                    {
                        var dataChangeStatus = data as object as product;
                        var changeStatusData = service.products.First(tmp => tmp.product_id == dataChangeStatus.product_id);
                        changeStatusData.product_status = dataChangeStatus.product_status;
                        service.SaveChanges();

                        check = service.products.Any(tmp => tmp.product_id == dataChangeStatus.product_id && tmp.product_status == dataChangeStatus.product_status);
                    }
                    break;

                case API.ProductStatusChangeStatus:
                    using (var service = new startcodingEntities())
                    {
                        var dataChangeStatus = data as object as product_status;
                        var changeStatusData = service.product_status.First(tmp => tmp.product_status_id == dataChangeStatus.product_status_id);
                        changeStatusData.product_status_status = dataChangeStatus.product_status_status;
                        service.SaveChanges();

                        check = service.product_status.Any(tmp => tmp.product_status_id == dataChangeStatus.product_status_id && tmp.product_status_status == dataChangeStatus.product_status_status);
                    }
                    break;
            }

            return check;
        }

        public static int GetId(API api, string data)
        {
            var id = 0;
            var service = new startcodingEntities();
            switch (api)
            {
                case API.AccountGetId:
                    id = service.accounts.First(tmp => tmp.account_email.Equals(data)).account_id;
                    break;

                case API.AccountLoginStatusGetId:
                    id = service.account_login_status.First(tmp => tmp.account_login_status_name.Equals(data)).account_login_status_id;
                    break;

                case API.AccountStatusGetId:
                    id = service.account_status.First(tmp => tmp.account_status_name.Equals(data)).account_status_id;
                    break;

                case API.AccountTypeGetId:
                    id = service.account_status.First(tmp => tmp.account_status_name.Equals(data)).account_status_id;
                    break;

                case API.BillsGetId:
                    id = service.bills.First(tmp => tmp.bill_time.Equals(data)).bill_id;
                    break;

                case API.BillsStatusGetId:
                    id = service.bill_status.First(tmp => tmp.bill_status_name.Equals(data)).bill_status_id;
                    break;

                case API.CategoryGetId:
                    id = service.categories.First(tmp => tmp.category_name.Equals(data)).category_id;
                    break;

                case API.DiscountGetId:
                    id = service.discounts.First(tmp => tmp.discount_name.Equals(data)).discount_id;
                    break;

                case API.PaymentModeGetId:
                    id = service.payment_mode.First(tmp => tmp.payment_mode_time.Equals(data)).payment_mode_id;
                    break;

                case API.ProductLevelGetId:
                    id = service.product_level.First(tmp => tmp.product_level_name.Equals(data)).product_level_id;
                    break;

                case API.ProductGetId:
                    id = service.products.First(tmp => tmp.product_title.Equals(data)).product_id;
                    break;

                case API.ProductStatusGetId:
                    id = service.product_status.First(tmp => tmp.product_status_name.Equals(data)).product_status_id;
                    break;
            }

            return id;
        }

        private static bool isExists(API api, string data)
        {
            var check = false;
            var service = new startcodingEntities();

            switch (api)
            {
                case API.AccountInsert:
                    check = service.accounts.Any(tmp => tmp.account_email.Equals(data));
                    break;

                case API.AccountLoginStatusInsert:
                    check = service.account_login_status.Any(tmp => tmp.account_login_status_name.Equals(data));
                    break;

                case API.AccountStatusInsert:
                    check = service.account_status.Any(tmp => tmp.account_status_name.Equals(data));
                    break;

                case API.AccountTypeInsert:
                    check = service.account_type.Any(tmp => tmp.account_type_name.Equals(data));
                    break;

                case API.BillsInsert:
                    check = service.bills.Any(tmp => tmp.bill_time == Convert.ToInt32(data));
                    break;

                case API.BillsStatusInsert:
                    check = service.bill_status.Any(tmp => tmp.bill_status_name.Equals(data));
                    break;

                case API.CategoryInsert:
                    check = service.categories.Any(tmp => tmp.category_name.Equals(data));
                    break;

                case API.DiscountInsert:
                    check = service.discounts.Any(tmp => tmp.discount_name.Equals(data));
                    break;

                case API.PaymentModeInsert:
                    check = service.payment_mode.Any(tmp => tmp.payment_mode_time == Convert.ToInt32(data));
                    break;

                case API.ProductLevelInsert:
                    check = service.product_level.Any(tmp => tmp.product_level_name.Equals(data));
                    break;

                case API.ProductInsert:
                    check = service.products.Any(tmp => tmp.product_title.Equals(data));
                    break;

                case API.ProductStatusInsert:
                    check = service.product_status.Any(tmp => tmp.product_status_name.Equals(data));
                    break;
            }

            return check;
        }
    }
}
