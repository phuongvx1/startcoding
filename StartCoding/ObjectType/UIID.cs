﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartCoding.ObjectType
{
    public enum UIID
    {
        AccountAddNewPane = 100,
        AccountViewAllPane = 101,
        AccountUpdatePane = 102,

        AccountLoginStatusAddNewPane = 201,
        AccountLoginStatusViewAllPane = 202,
        AccountLoginStatusUpdatePane = 203,
        AccountLoginStatusLayoutPane = 204,

        AccountStatusAddNewPane = 301,
        AccountStatusViewAllPane = 302,
        AccountStatusUpdatePane = 303,
        AccountStatusLayoutPane = 304,

        AccountTypeAddNewPane = 401,
        AccountTypeViewAllPane = 402,
        AccountTypeUpdatePane = 403,
        AccountTypeLayoutPane = 404,

        BillsViewAllPane = 500,

        BillStatusAddNewPane = 600,
        BillStatusViewAllPane = 601,
        BillStatusUpdatePane = 602,
        BillStatusLayoutPane = 603,

        CategoryAddNewPane = 700,
        CategoryViewAllPane = 701,
        CategoryUpdatePane = 702,
        CategoryLayoutPane = 703,

        DashboardLayoutPane = 800,

        DiscountAddNewPane = 900,
        DiscountViewAllPane = 901,
        DiscountUpdatePane = 902,
        DiscountLayoutPane = 903,

        PaymentModeAddNewPane = 1000,
        PaymentModeViewAllPane = 1001,
        PaymentModeUpdatePane = 1002,
        PaymentModeLayoutPane = 1003,

        ProductLevelAddNewPane = 1100,
        ProductLevelViewAllPane = 1101,
        ProductLevelUpdatePane = 1102,
        ProductLevelLayoutPane = 1103,

        ProductAddNewPane = 1200,
        ProductViewAllPane = 1201,
        ProductUpdatePane = 1202,

        ProductStatusAddNewPane = 1300,
        ProductStatusViewAllPane = 1301,
        ProductStatusUpdatePane = 1302,
        ProductStatusLayoutPane = 1303,
    }
}
