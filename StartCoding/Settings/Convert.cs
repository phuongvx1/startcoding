﻿using StartCoding.ObjectType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartCoding.Settings
{
    public class Convert
    {
        public static string GetObjectType(string data)
        {
            return data.Substring(data.LastIndexOf(" "), data.Length - data.LastIndexOf(" ")).TrimStart();
        }
    }
}
