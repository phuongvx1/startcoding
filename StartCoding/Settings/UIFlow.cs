﻿using StartCoding.ObjectType;
using StartCoding.Views.Admin.Account;
using StartCoding.Views.Admin.AccountLoginStatus;
using StartCoding.Views.Admin.AccountStatus;
using StartCoding.Views.Admin.AccountType;
using StartCoding.Views.Admin.Bills;
using StartCoding.Views.Admin.BillsStatus;
using StartCoding.Views.Admin.BillStatus;
using StartCoding.Views.Admin.Category;
using StartCoding.Views.Admin.Dashboard;
using StartCoding.Views.Admin.Discounts;
using StartCoding.Views.Admin.PaymentMode;
using StartCoding.Views.Admin.ProductLevel;
using StartCoding.Views.Admin.Products;
using StartCoding.Views.Admin.ProductStatus;
using System.Windows.Controls;

namespace StartCoding.Settings
{
    public class UIFlow
    {
        public static void Switch(Canvas canvas, UserControl data)
        {
            canvas.Children.Clear();
            canvas.Children.Add(data);
        }

        public static void AddPane(UIID paneId,Canvas canvas)
        {
            switch (paneId)
            {
                //account
                case UIID.AccountAddNewPane:
                    AddPane(canvas, new AccountsAddNew());
                    break;
                case UIID.AccountViewAllPane:
                    AddPane(canvas, new AccountsViewAll());
                    break;
                case UIID.AccountUpdatePane:
                    AddPane(canvas, new AccountsUpdate());
                    break;

                //account login status
                case UIID.AccountLoginStatusAddNewPane:
                    AddPane(canvas, new AccountLoginStatusAddNew());
                    break;
                case UIID.AccountLoginStatusViewAllPane:
                    AddPane(canvas, new AccountLoginStatusViewAll());
                    break;
                case UIID.AccountLoginStatusUpdatePane:
                    AddPane(canvas, new AccountLoginStatusUpdate());
                    break;
                case UIID.AccountLoginStatusLayoutPane:
                    AddPane(canvas, new AccountLoginStatusLayout());
                    break;

                //account status
                case UIID.AccountStatusAddNewPane:
                    AddPane(canvas, new AccountStatusAddNew());
                    break;
                case UIID.AccountStatusViewAllPane:
                    AddPane(canvas, new AccountStatusViewAll());
                    break;
                case UIID.AccountStatusUpdatePane:
                    AddPane(canvas, new AccountStatusUpdate());
                    break;
                case UIID.AccountStatusLayoutPane:
                    AddPane(canvas, new AccountStatusLayout());
                    break;

                //account type
                case UIID.AccountTypeAddNewPane:
                    AddPane(canvas, new AccountTypeAddNew());
                    break;
                case UIID.AccountTypeViewAllPane:
                    AddPane(canvas, new AccountTypeViewAll());
                    break;
                case UIID.AccountTypeUpdatePane:
                    AddPane(canvas, new AccountTypeUpdate());
                    break;
                case UIID.AccountTypeLayoutPane:
                    AddPane(canvas, new AccountTypeLayout());
                    break;

                //bills
                case UIID.BillsViewAllPane:
                    AddPane(canvas, new BillsViewAll());
                    break;

                //bills status
                case UIID.BillStatusAddNewPane:
                    AddPane(canvas, new BillStatusAddNew());
                    break;
                case UIID.BillStatusViewAllPane:
                    AddPane(canvas, new BillStatusViewAll());
                    break;
                case UIID.BillStatusUpdatePane:
                    AddPane(canvas, new BillStatusUpdate());
                    break;
                case UIID.BillStatusLayoutPane:
                    AddPane(canvas, new BillStatusLayout());
                    break;

                //category
                case UIID.CategoryAddNewPane:
                    AddPane(canvas, new CategoryAddNew());
                    break;
                case UIID.CategoryViewAllPane:
                    AddPane(canvas, new CategoryViewAll());
                    break;
                case UIID.CategoryUpdatePane:
                    AddPane(canvas, new CategoryUpdate());
                    break;
                case UIID.CategoryLayoutPane:
                    AddPane(canvas, new CategoryLayout());
                    break;

                //dashboard
                case UIID.DashboardLayoutPane:
                    AddPane(canvas, new DashboardLayout());
                    break;

                //discount
                case UIID.DiscountAddNewPane:
                    AddPane(canvas, new DiscountAddNew());
                    break;
                case UIID.DiscountViewAllPane:
                    AddPane(canvas, new DiscountViewAll());
                    break;
                case UIID.DiscountUpdatePane:
                    AddPane(canvas, new DiscountUpdate());
                    break;
                case UIID.DiscountLayoutPane:
                    AddPane(canvas, new DiscountLayout());
                    break;

                //payment mode
                case UIID.PaymentModeAddNewPane:
                    AddPane(canvas, new PaymentModeAddNew());
                    break;
                case UIID.PaymentModeViewAllPane:
                    AddPane(canvas, new PaymentModeViewAll());
                    break;
                case UIID.PaymentModeUpdatePane:
                    AddPane(canvas, new PaymentModeUpdate());
                    break;
                case UIID.PaymentModeLayoutPane:
                    AddPane(canvas, new PaymentModeLayout());
                    break;

                //product level
                case UIID.ProductLevelAddNewPane:
                    AddPane(canvas, new ProductLevelAddNew());
                    break;
                case UIID.ProductLevelViewAllPane:
                    AddPane(canvas, new ProductLevelViewAll());
                    break;
                case UIID.ProductLevelUpdatePane:
                    AddPane(canvas, new ProductLevelUpdate());
                    break;
                case UIID.ProductLevelLayoutPane:
                    AddPane(canvas, new ProductLevelLayout());
                    break;

                //product
                case UIID.ProductAddNewPane:
                    AddPane(canvas, new ProductsAddNew());
                    break;
                case UIID.ProductViewAllPane:
                    AddPane(canvas, new ProductsViewAll());
                    break;
                case UIID.ProductUpdatePane:
                    AddPane(canvas, new ProductsUpdate());
                    break;
                
                //product status
                case UIID.ProductStatusAddNewPane:
                    AddPane(canvas, new ProductsStatusAddNew());
                    break;
                case UIID.ProductStatusViewAllPane:
                    AddPane(canvas, new ProductsStatusViewAll());
                    break;
                case UIID.ProductStatusUpdatePane:
                    AddPane(canvas, new ProductsStatusUpdate());
                    break;
                case UIID.ProductStatusLayoutPane:
                    AddPane(canvas, new ProductsStatusLayout());
                    break;
            }
        }

        private static void AddPane(Canvas canvas, UserControl data)
        {
            canvas.Children.Add(data);
        }

        public static void UIControler(string uiid,Canvas canvas)
        {
            switch (uiid)
            {
                case "Dashboard":
                    Switch(canvas, new DashboardLayout());
                    break;
                case "Products":
                    Switch(canvas, new ProductsLayout());
                    break;
                case "Bills":
                    Switch(canvas, new BillsLayout());
                    break;
                case "Accounts":
                    Switch(canvas, new AccountsLayout());
                    break;
            }
        }
    }
}
